package com.module;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;
import com.miteksystems.misnap.analyzer.MiSnapAnalyzerResult;
import com.miteksystems.misnap.misnapworkflow.MiSnapWorkflowActivity;
import com.miteksystems.misnap.misnapworkflow_UX2.MiSnapWorkflowActivity_UX2;
import com.miteksystems.misnap.misnapworkflow_UX2.params.WorkflowApi;
import com.miteksystems.misnap.params.BarcodeApi;
import com.miteksystems.misnap.params.CameraApi;
import com.miteksystems.misnap.params.CreditCardApi;
import com.miteksystems.misnap.params.MiSnapApi;
import com.miteksystems.misnap.params.ScienceApi;
import com.miteksystems.facialcapture.workflow.FacialCaptureWorkflowActivity;
import com.miteksystems.facialcapture.workflow.api.FacialCaptureResult;
import com.miteksystems.facialcapture.workflow.params.FacialCaptureWorkflowApi;
import com.module.model.MiSnapResponseBuilder;
import com.miteksystems.misnap.utils.Utils;


import org.json.JSONException;
import org.json.JSONObject;
import com.facebook.react.bridge.ReadableMap;

import java.util.List;

@ReactModule(name = MiSnapModule.NAME)
public class MiSnapModule extends ReactContextBaseJavaModule {

    public static final String NAME = "MiSnapModule";

    private Promise miSnapPromise;
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static long mTime;
    private static int mUxWorkflow = 2;
    private static int mGeoRegion;
    private static final long PREVENT_DOUBLE_CLICK_TIME_MS = 1000;
    private static int ocrMode = ScienceApi.REQUEST_OCR_NONE;

    public MiSnapModule(ReactApplicationContext reactContext) {
        super(reactContext);

        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

    @ReactMethod
    public void captureDriversLicenseFront(ReadableMap opts, Promise promise) {
        miSnapPromise = promise;
        mGeoRegion = ScienceApi.GEO_REGION_US;
        startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_DRIVER_LICENSE);
    }

    @ReactMethod
    public void captureDriversLicenseBack(ReadableMap opts, Promise promise) {
        miSnapPromise = promise;
        startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_PDF417);
    }

    @ReactMethod
    public void capturePassport(ReadableMap opts, Promise promise) {
        miSnapPromise = promise;
        startMiSnapWorkflow(MiSnapApi.PARAMETER_DOCTYPE_PASSPORT);
    }

    @ReactMethod
    public void captureSelfie(ReadableMap opts, Promise promise) {
        miSnapPromise = promise;
        startMiSnapWorkflow("SELFIE");
    }




    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {

        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            Log.i(NAME, "************* onActivityResult  ***********");
            if (RESULT_OK == resultCode) {

                if (data != null) {

                    Bundle extras = data.getExtras();
                    String miSnapResultCode = extras.getString(MiSnapApi.RESULT_CODE);
                    String mibi = "";
                    String docType = "";
                    mibi = extras.getString(MiSnapApi.RESULT_MIBI_DATA);

                    try {
                      docType = new JSONObject(mibi).getString("Document");
                    } catch (JSONException e) {
                      e.printStackTrace();
                    }
                    if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_CAMERA_ONLY)) {
                      miSnapResultCode = "SELFIE";
                    }


                    switch (miSnapResultCode) {

                        // MiSnap check capture
                        case MiSnapApi.RESULT_SUCCESS_VIDEO:
                        case MiSnapApi.RESULT_SUCCESS_STILL:
                            Log.i(NAME, "MIBI: " + extras.getString(MiSnapApi.RESULT_MIBI_DATA));

                            // Image returned successfully
                            byte[] sImage = data.getByteArrayExtra(MiSnapApi.RESULT_PICTURE_DATA);
                            byte[] sEncodedImage = Base64.encode(sImage, Base64.DEFAULT);

                            List<String> warnings = extras.getStringArrayList(MiSnapApi.RESULT_WARNINGS);

                            if (warnings != null && !warnings.isEmpty()) {
                                String message = "WARNINGS:";
                                if ((warnings.contains(MiSnapAnalyzerResult.FrameChecks.WRONG_DOCUMENT.name()))) {
                                    message += "\nWrong document detected";
                                }

                                miSnapPromise.reject(MiSnapApi.RESULT_ERROR_SDK_STATE_ERROR, message);
                            }

                            miSnapPromise.resolve(MiSnapResponseBuilder.Builder.newInstance()
                                    .setType(MiSnapResponseBuilder.SUCCESS)
                                    .setEncodedImage(sImage)
                                    .setOriginalImage(sImage)
                                    .setResults(mibi).build().result());

                            break;


                        // Barcode capture
                        case MiSnapApi.RESULT_SUCCESS_PDF417:

                            miSnapPromise.resolve(MiSnapResponseBuilder.Builder.newInstance()
                            .setType(MiSnapResponseBuilder.SUCCESS)
                            .setPDF417Code(data.getExtras().getString(BarcodeApi.RESULT_PDF417_DATA))
                            .setResults(mibi).build().result());

                            break;

                        case "SELFIE":
                            FacialCaptureResult result = extras.getParcelable(FacialCaptureWorkflowApi.FACIAL_CAPTURE_RESULT);
                            byte[] selfieEncodedImage = result.getImage();

                            miSnapPromise.resolve(MiSnapResponseBuilder.Builder.newInstance()
                              .setType(MiSnapResponseBuilder.SUCCESS)
                              .setEncodedImage(selfieEncodedImage)
                              .setOriginalImage(selfieEncodedImage)
                              .setResults(mibi).build().result());

                            break;

                    }
                } else {
                    // Image canceled, stop
                    miSnapPromise.reject(MiSnapResponseBuilder.ERROR_CANCELED, "MiSnap canceled");
                }
            } else if (RESULT_CANCELED == resultCode) {
                // Camera not working or not available, stop
              Log.i(NAME, "************* RESULT_CANCELED  ***********");

              if (data != null) {
                    Bundle extras = data.getExtras();
                    String miSnapResultCode = extras.getString(MiSnapApi.RESULT_CODE);
                    if (!miSnapResultCode.isEmpty()) {
                        miSnapPromise.reject(MiSnapResponseBuilder.ERROR_OTHER, "Shutdown reason: " + miSnapResultCode);
                    }
                } else {
                    miSnapPromise.reject(MiSnapResponseBuilder.ERROR_CANCELED, "Operation canceled!!!");
                }
            }

        }
    };

    public void startMiSnapWorkflow(String docType) {

        if (System.currentTimeMillis() - mTime < PREVENT_DOUBLE_CLICK_TIME_MS) {
            Log.e("UxStateMachine", "Double-press detected");
            return;
        }

        mTime = System.currentTimeMillis();
        JSONObject misnapParams = new JSONObject();

        try {

            misnapParams.put(MiSnapApi.MiSnapDocumentType, docType);

            if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT)) {
                misnapParams.put(ScienceApi.MiSnapGeoRegion, mGeoRegion);
            }

            if (docType.equals(MiSnapApi.PARAMETER_DOCTYPE_BARCODES)) {
                misnapParams.put(BarcodeApi.BarCodeTypes, BarcodeApi.BARCODE_ALL - BarcodeApi.BARCODE_CODE_128);
            }

            misnapParams.put(CameraApi.MiSnapAllowScreenshots, 1);
            if (!docType.equals("SELFIE")) {
              misnapParams.put(ScienceApi.MiSnapRequestOCR, ocrMode);
              misnapParams.put(WorkflowApi.MiSnapTrackGlare, WorkflowApi.TRACK_GLARE_ENABLED);
            }

        } catch (JSONException e) {

            miSnapPromise.reject(MiSnapApi.RESULT_ERROR_SDK_STATE_ERROR, MiSnapResponseBuilder.Builder.newInstance()
                    .setType(MiSnapResponseBuilder.ERROR)
                    .setMessage(e.getMessage())
                    .setCode(MiSnapApi.RESULT_ERROR_SDK_STATE_ERROR).build().result());
        }

        startMinSnapActivity(misnapParams);
    }

    private void startMinSnapActivity(JSONObject misnapParams) {

        Activity currentActivity = getCurrentActivity();

        if (currentActivity == null) {
            miSnapPromise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        Intent intentMiSnap = new Intent(getCurrentActivity(), MiSnapWorkflowActivity_UX2.class);

        if (mUxWorkflow == 1) {
            intentMiSnap = new Intent(getCurrentActivity(), MiSnapWorkflowActivity.class);
        }

        try {
          if (misnapParams.getString(MiSnapApi.MiSnapDocumentType).equals("SELFIE")) {
              intentMiSnap = new Intent(currentActivity, FacialCaptureWorkflowActivity.class);
          }
        } catch (JSONException e) {
              e.printStackTrace();
        }

        intentMiSnap.putExtra(MiSnapApi.JOB_SETTINGS, misnapParams.toString());
        currentActivity.startActivityForResult(intentMiSnap, MiSnapApi.RESULT_PICTURE_CODE);
    }

}
