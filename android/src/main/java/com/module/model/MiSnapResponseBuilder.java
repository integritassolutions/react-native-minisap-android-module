package com.module.model;

import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import java.util.Base64;

public class MiSnapResponseBuilder {

    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";

    public static final String ERROR_CANCELED = "CANCELED";
    public static final String ERROR_NO_PERMISSION = "NO PERMISSION";
    public static final String ERROR_OTHER = "OTHER";

    private final String type;
    private final String code;
    private final String message;
    private final String results;
    private final String encodedImage;
    private final String originalImage;
    private final String PDF417Code;

    public MiSnapResponseBuilder(Builder builder) {
        this.type = builder.type;
        this.code = builder.code;
        this.message = builder.message;
        this.results = builder.results;
        this.encodedImage = builder.encodedImage;
        this.originalImage = builder.originalImage;
        this.PDF417Code = builder.PDF417Code;
    }

    public WritableMap result() {
        WritableMap resultData = new WritableNativeMap();

        if(type.equals(SUCCESS) && PDF417Code != null){
          resultData.putString("results", results);
          resultData.putString("PDF417Code", PDF417Code);
        }
        else if (type.equals(SUCCESS)) {
            resultData.putString("results", results);
            resultData.putString("encodedImage", encodedImage.toString());
            resultData.putString("originalImage", originalImage.toString());
        }

        if (type.equals(ERROR)) {

            resultData.putString("type", code);
            resultData.putString("message", message);
        }

        return resultData;
    }

    public static class Builder {

        private String type;
        private String code;
        private String message;
        private String results;
        private String encodedImage;
        private String originalImage;
        private String PDF417Code;

        public static Builder newInstance() {
            return new Builder();
        }

        private Builder() {
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setCode(String code) {
            this.code = code;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setResults(String results) {
            this.results = results;
            return this;
        }

        public Builder setEncodedImage(byte[] encodedImage) {
            this.encodedImage = Base64.getEncoder().encodeToString(encodedImage);
            return this;
        }

        public Builder setOriginalImage(byte[] originalImage) {
            this.originalImage = Base64.getEncoder().encodeToString(originalImage);
            return this;
        }

        public Builder setPDF417Code(String PDF417Code) {
            this.PDF417Code = PDF417Code;
            return this;
        }

        public MiSnapResponseBuilder build() {
            return new MiSnapResponseBuilder(this);
        }
    }

}
