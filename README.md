# react-native-minisap-module

Minisap SDK Module

## Installation

```sh
npm install react-native-minisap-module
```

## Usage

```js
import MinisapModule from "react-native-minisap-module";

// ...

const result = await MinisapModule.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
