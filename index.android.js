import { NativeModules } from 'react-native';
const module = NativeModules.MinisapModule;
export default {
  captureDriversLicenseFront: module.captureDriversLicenseFront,
  captureDriversLicenseBack: module.captureDriversLicenseBack,
  capturePassport: module.capturePassport,
  captureSelfie: module.captureSelfie
}